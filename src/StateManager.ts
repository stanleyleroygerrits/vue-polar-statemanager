import StateManagerModule from './StateManagerModule'
import {
    ActionList,
    GetterList,
    MutationList,
    StateManagerModuleList,
    StateManagerModuleNamespaceList, StateManagerNameSpace
} from "./Types";

class StateManager
{
    modules: StateManagerModuleList = [];
    moduleNamespaces: StateManagerModuleNamespaceList = [];

    getterTree: GetterList = [];
    mutationTree: MutationList = [];
    actionTree: ActionList = [];

    public registerModule(stateManagerModule: StateManagerModule)
    {
        stateManagerModule.register();
        this.modules.push(stateManagerModule);
        this.moduleNamespaces.push(stateManagerModule.namespace);
    }

    public getModule(namespace: StateManagerNameSpace)
    {
        const index = this.moduleNamespaces.indexOf(namespace);

        if(index !== -1 && this.modules[index])
        {
            return this.modules[index];
        }

        throw Error('No State module found for: '+namespace);
    }

    public dispatch(namespace: string, action: string,params = []): boolean
    {
        return this.getModule(namespace).dispatch(action,params);
    }

    public get(namespace: string, getter: string,...params): any
    {
        return this.getModule(namespace)[getter](...params)
    }

    public Decorators = {
        Getter: (target: any, propertyKey: string) => {
            this.getterTree.push({
                namespace: propertyKey,
                methodName: target.constructor.name
            });
        },

        Module: (target: any) => {
            this.registerModule(new target());
        },

        Mutation: (target: any, propertyKey: string,descriptor: PropertyDescriptor) => {
            this.mutationTree.push({
                namespace: propertyKey,
                methodName: target.constructor.name
            });
        },

        Action: (target: any, propertyKey: string) => {
            this.actionTree.push({
                namespace: propertyKey,
                methodName: target.constructor.name
            });
        },
    }
}

export default new StateManager();
