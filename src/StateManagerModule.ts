import {Vue as _Vue} from "vue/types/vue";
import Vue from "vue";

export default abstract class StateManagerModule<S = {}>
{
    abstract  namespace: string;
    protected abstract state: S | _Vue;

    public register(): void
    {
        this.state = new Vue({data: this.state});
    }

    public dispatch(action: string,...params): boolean
    {
        if(!this[action]){
            return false;
        }

        return this[action](...params);
    }

    public get(getter: string,...params): any
    {
        return this[getter](...params)
    }

    public Decorators = {
        Getter: (...params) => {
            const name: string = params[0];
            return (target, propertyKey: string) => {
                target[propertyKey] = (...params) => this.get((name) ? name : propertyKey,...params)
            }
        },
        Action: (...params) => {
            const name: string = params[0];
            return (target, propertyKey: string) => {
                target[propertyKey] = (...params) => this.dispatch((name) ? name : propertyKey,...params)
            }
        }
    }
}
