import StateManagerModule from "./StateManagerModule";

export type StateManagerModuleList = Array<StateManagerModule>;
export type StateManagerModuleNamespaceList = Array<string>;
export type StateManagerNameSpace = string;

export interface Getter
{
    namespace: StateManagerNameSpace;
    methodName: string;
}

export type GetterList = Array<Getter>;

export interface Mutation
{
    namespace: StateManagerNameSpace;
    methodName: string;
}

export type MutationList = Array<Mutation>;

export interface Action
{
    namespace: StateManagerNameSpace;
    methodName: string;
}

export type ActionList = Array<Action>;
